from wtforms.form import Form as BaseForm
from wtforms import fields
from wtforms import validators
from wtforms import ValidationError

from trashbin.models import User

class FieldTaken(object):
    def __init__(self, column, message='That is already in use'):
        self.column = column
        self.message = message

    def __call__(self, form, field):
        db = form.request.db

        if db.query(self.column).filter(self.column == field.data).first():
            raise ValidationError(self.message)

field_taken = FieldTaken

class Form(BaseForm):
    def __init__(self, request, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.request = request

class RegisterForm(Form):
    username = fields.StringField('Username', [
        field_taken(User.username)
    ])
    email = fields.StringField('Email', [
        validators.email(),
        field_taken(User.email)
    ])
    password = fields.PasswordField('Password', [
        validators.required(),
        validators.equal_to('confirm_password')
    ])
    confirm_password = fields.PasswordField('Confirm Password')

class LoginForm(Form):
    username = fields.StringField('Username')
    password = fields.PasswordField('Password')

class PasteForm(Form):
    title = fields.StringField('Title')
    content = fields.TextAreaField('Content')
