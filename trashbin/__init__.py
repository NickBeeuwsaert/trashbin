import datetime

from pyramid.authentication import AuthTktAuthenticationPolicy
from pyramid.authorization import ACLAuthorizationPolicy
from pyramid.config import Configurator
from pyramid.renderers import JSON
from pyramid.session import SignedCookieSessionFactory
from pyramid.view import view_config

from trashbin.models import User


@view_config(route_name='index', renderer='index.jinja2')
def index(request):
    return {}

def auth_tkt(userid, request):
    user = request.db.query(User).filter(User.username == userid).first()

    if not user: return None

    return []

def main(global_config, **settings):
    authz_policy = ACLAuthorizationPolicy()
    authn_policy = AuthTktAuthenticationPolicy(
        settings['auth.secret'],
        callback=auth_tkt
    )
    session_factory = SignedCookieSessionFactory(settings['session.secret'])

    config = Configurator(
        settings=settings,
        authentication_policy=authn_policy,
        authorization_policy=authz_policy,
        session_factory=session_factory
    )

    config.add_renderer('json', JSON(
        adapters=[
            (datetime.datetime, lambda o, r: o.isoformat()),
            (datetime.date, lambda o, r: o.isoformat()),
            (datetime.time, lambda o, r: o.isoformat()),
        ]
    ))

    config.include('trashbin.models')
    config.add_request_method(
        lambda r: r.db.query(User).filter(User.username == r.authenticated_userid).first(),
        'user',
        reify=True
    )

    config.add_static_view(name='static', path='trashbin:static')

    config.scan(__name__)
    config.add_route('index', '/')

    config.include('trashbin.views.user', route_prefix='/user')
    config.include('trashbin.views.paste', route_prefix='/paste')

    return config.make_wsgi_app()