from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config

from trashbin.forms import PasteForm
from trashbin.models import Paste
from trashbin.resources import PasteFactory


@view_config(
    route_name='paste.create',
    permission='create',
    request_method=['GET', 'POST'],
    renderer='paste/create.jinja2'
)
def create(request):
    form = PasteForm(request, request.POST)

    if request.method == 'POST' and form.validate():
        paste = Paste(
            title=form.title.data,
            content=form.content.data,
            user=request.user
        )
        request.db.add(paste)
        request.db.flush()

        return HTTPFound(
            location=request.route_path('paste.view', id=paste.id)
        )

    return dict(
        form=form
    )

@view_config(
    route_name='paste.view',
    request_method='GET',
    renderer='paste/view.jinja2'
)
def view(request):
    return dict(
        paste=request.context
    )

@view_config(route_name='paste.update', request_method='POST')
def update(request):
    return {}

@view_config(route_name='paste.delete', request_method='DELETE')
def delete(request):
    return {}

def includeme(config):
    config.scan(__name__)

    config.add_route(
        'paste.create',
        '/',
        factory=PasteFactory
    )
    config.add_route(
        'paste.view', '/{id}',
        factory=PasteFactory,
        traverse='/{id}'
    )
    config.add_route(
        'paste.update', '/{id}/update',
        factory=PasteFactory,
        traverse='/{id}'
    )
    config.add_route(
        'paste.delete', '/{id}/delete',
        factory=PasteFactory,
        traverse='{id}'
    )
