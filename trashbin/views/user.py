from pyramid.httpexceptions import HTTPFound
from pyramid.security import remember, forget
from pyramid.view import view_config

from trashbin.forms import LoginForm
from trashbin.forms import RegisterForm
from trashbin.models import User
from trashbin.resources import UserFactory


@view_config(route_name='user.login', renderer='user/login.jinja2')
def login(request):
    form = LoginForm(request, request.POST)


    if request.method == 'POST' and form.validate():
        user = request.db.query(User).filter(User.username == form.username.data).first()

        if user and user.verify_password(form.password.data):
            return HTTPFound(
                headers=remember(request, user.username),
                location=request.route_path('index')
            )

    return dict(
        form=form
    )

@view_config(route_name='user.register', renderer='user/register.jinja2')
def register(request):
    form = RegisterForm(request, request.POST)

    if request.method == 'POST' and form.validate():
        user = User(
            username=form.username.data,
            email=form.email.data
        )
        user.set_password(form.password.data)

        request.db.add(user)

        return HTTPFound(
            location=request.route_path('user.login')
        )

    return dict(
        form=form
    )

@view_config(route_name='user.logout')
def logout(request):
    return HTTPFound(
        headers=forget(request),
        location=request.route_path('index')
    )

@view_config(route_name='user.view', renderer='json')
def view(request):
    return dict(
        user=request.context
    )

def includeme(config):
    config.scan(__name__)

    config.add_route('user.login', '/login')
    config.add_route('user.register', '/register')
    config.add_route('user.logout', '/logout')
    config.add_route(
        'user.view', '/view/{user}',
        factory=UserFactory,
        traverse='/{user}'
    )
