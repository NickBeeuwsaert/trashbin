from pyramid.security import Allow, Deny
from pyramid.security import Everyone, Authenticated

from trashbin.models import Paste
from trashbin.models import User


class UserFactory(object):
    __acl__ = [
        (Allow, Authenticated, 'view')
    ]

    def __init__(self, request):
        self.request = request

    def __getitem__(self, userid):
        db = self.request.db
        user = db.query(User).filter(User.username == userid).first()

        if not user:
            raise KeyError

        return user


class PasteFactory(object):
    __acl__ = [
        (Allow, Everyone, 'view'),
        (Allow, Authenticated, 'create')
    ]

    def __init__(self, request):
        self.request = request

    def __getitem__(self, pasteid):
        db = self.request.db
        paste = db.query(Paste).filter(Paste.id == pasteid).first()

        if not paste:
            raise KeyError

        return paste
