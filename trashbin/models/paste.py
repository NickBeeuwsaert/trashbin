from datetime import datetime

import sqlalchemy as sa
from sqlalchemy.orm import relationship

from trashbin.models.meta import Base


class Paste(Base):
    __tablename__ ='pastes'

    id = sa.Column(sa.Integer, primary_key=True)
    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'))
    title = sa.Column(sa.String(256))
    content = sa.Column(sa.String)
    created_at = sa.Column(sa.DateTime, default=datetime.now)
    modified_at = sa.Column(sa.DateTime, default=datetime.now, onupdate=datetime.now)

    user = relationship("User", back_populates="pastes")

    def __acl__(self):
        return []

    def __json__(self, request):
        return dict(
            id=self.id,
            user=self.user,
            title=self.title,
            content=self.content,
            created_at=self.created_at,
            modified_at=self.modified_at
        )