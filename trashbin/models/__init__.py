from sqlalchemy import engine_from_config
from sqlalchemy.orm import sessionmaker
import zope.sqlalchemy

# Import models for easy importing
from trashbin.models.user import User
from trashbin.models.paste import Paste


__all__ = [
    User,
    Paste
]

def get_session(session_factory, tm):
    session = session_factory()

    zope.sqlalchemy.register(session, transaction_manager=tm)

    return session

def includeme(config):
    settings = config.get_settings()
    engine = engine_from_config(settings)
    Session = sessionmaker(bind=engine)

    config.add_request_method(
        lambda r: get_session(Session, r.tm),
        'db',
        reify=True
    )