from datetime import datetime

from passlib.apps import custom_app_context as pwd_context
import sqlalchemy as sa
from sqlalchemy.orm import relationship

from trashbin.models.meta import Base



class User(Base):
    __tablename__ = 'users'

    id = sa.Column(sa.Integer, primary_key=True)
    username = sa.Column(sa.Text, unique=True)
    email = sa.Column(sa.String(256), unique=True)
    password = sa.Column(sa.String(100))
    created_at = sa.Column(sa.DateTime, default=datetime.now)
    modified_at = sa.Column(sa.DateTime, onupdate=datetime.now, default=datetime.now)

    pastes = relationship("Paste", back_populates="user")



    def verify_password(self, password):
        return pwd_context.verify(password, self.password)

    def set_password(self, password):
        self.password = pwd_context.encrypt(password)


    def __acl__(self):
        return []

    def __json__(self, request):
        return dict(
            id=self.id,
            username=self.username,
            created_at=self.created_at,
            modified_at=self.modified_at
        )