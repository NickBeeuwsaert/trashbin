"""Create pastes table

Revision ID: ec84f2be3dab
Revises: 1d74d8462bb8
Create Date: 2016-07-15 20:06:54.343008

"""

# revision identifiers, used by Alembic.
revision = 'ec84f2be3dab'
down_revision = '1d74d8462bb8'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
        'pastes',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('user_id', sa.Integer, sa.ForeignKey('users.id')),
        sa.Column('title', sa.String(256)),
        sa.Column('content', sa.String),
        sa.Column('created_at', sa.DateTime),
        sa.Column('modified_at', sa.DateTime)
    )


def downgrade():
    op.drop_table('pastes')
