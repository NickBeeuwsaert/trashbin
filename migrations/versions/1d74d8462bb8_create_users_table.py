"""Create users table

Revision ID: 1d74d8462bb8
Revises: 
Create Date: 2016-07-15 19:26:28.248132

"""

# revision identifiers, used by Alembic.
revision = '1d74d8462bb8'
down_revision = None
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
        'users',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('username', sa.Text, unique=True),
        sa.Column('email', sa.String(256), unique=True),
        sa.Column('password', sa.String(100)),
        sa.Column('created_at', sa.DateTime),
        sa.Column('modified_at', sa.DateTime)
    )


def downgrade():
    op.drop_table('users')
