from setuptools import setup


install_requires = [
    'pyramid',
    'pyramid-jinja2',
    'pyramid-tm',
    'SQLAlchemy',
    'zope.sqlalchemy',
    'passlib'
]

setup(
    name='trashbin',
    version='0.0.0',
    author='Nick Beeuwsaert',
    license='MIT',
    packages=[
        'trashbin'
    ],
    install_requires=install_requires,
    extras_require={
        'dev': [
            'alembic'
        ]
    },
    entry_points={
        'paste.app_factory': [
            'main = trashbin:main'
        ]
    }
)